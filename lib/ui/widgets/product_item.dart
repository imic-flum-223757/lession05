import 'package:flutter/material.dart';

class ProductItem extends StatelessWidget {
  final dynamic product;

  const ProductItem({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {print(product['id'])},
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child:  IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 100,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    border: Border.all(width: 1, color: Color(0xFFB74093)),
                    image: DecorationImage(
                        image: AssetImage('assets/images/${product['img']}'),
                        fit: BoxFit.cover)),
              ),
              Expanded(
                  child: Container(
                    color: Colors.red,
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '${product['name']}',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [Text('${product['price']}'), Text('x1')],
                        ),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
