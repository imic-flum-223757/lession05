import 'package:flutter/material.dart';
import 'package:lession05/ui/widgets/product_item_clip.dart';
import 'package:lession05/view_model/cart_vm.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cartModel = Provider.of<CartViewModel>(context);
    return Scaffold(
      body: ListView(
        children: List.generate(cartModel.products.length,
            (index) => ProductItemClip(product: cartModel.products[index])),
      ),
    );
  }
}
