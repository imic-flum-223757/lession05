import 'package:flutter/material.dart';
import 'package:lession05/ui/screens/home_widget.dart';

class MyHomePage extends StatelessWidget with HomeWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: SafeArea(
          child: Column(
            children: [
              buildHeaderText(context),
              buildListItems(context)
            ],
          ),
        ),
      ),
    );
  }
}
