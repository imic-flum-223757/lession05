import 'package:flutter/material.dart';
import 'package:lession05/base/provider_view.dart';
import 'package:lession05/view_model/news_vm.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ProviderView<NewsViewModel>(
        model: NewsViewModel(),
        onReady: (model) => model.onLoading(),
        builder: ((_, model, __) {
          if (model.loading) {
            return Center(
              child: Text('Loading...'),
            );
          } else {
            if (model.allNews.length > 0) {
              return SmartRefresher(
                controller: model.refreshController,
                enablePullDown: true,
                enablePullUp: true,
                onRefresh: () => model.onRefresh(),
                onLoading: () => model.onLoading(),
                child: ListView.builder(
                    itemCount: model.allNews.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        title: Text('${model.allNews.elementAt(index)['title']}'),
                      );
                    }),
              );
            }
            return Center(
              child: Text("""Don't have content"""),
            );
          }
        }),
      ),
    );
  }
}
