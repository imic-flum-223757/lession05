import 'package:http/http.dart';
import 'package:lession05/helper/constants.dart';

class NewsRepository {
  Future<Response> getNews(int page) {
    return get(urlAPINews + '/news/$page.json');
  }
}
