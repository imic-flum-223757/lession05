import 'package:flutter/material.dart';
import 'package:lession05/ui/screens/home_news.dart';
import 'package:lession05/ui/screens/home_screen.dart';
import 'package:lession05/view_model/cart_vm.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<CartViewModel>(create: (_) => CartViewModel())
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: HomeNews(),
        ));
  }
}
