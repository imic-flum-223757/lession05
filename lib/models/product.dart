class Product {
  int id;
  String img;
  String name;
  String price;

  Product({this.id, this.img, this.name, this.price});

  factory Product.fromJson(Map<String, dynamic> json) {
    var product = Product(
        id: json['id'],
        img: json['img'],
        name: json['name'],
        price: json['price']);
    return product;
  }
}
