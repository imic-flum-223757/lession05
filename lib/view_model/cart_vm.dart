
import 'package:flutter/material.dart';
import 'package:lession05/models/product.dart';

class CartViewModel extends ChangeNotifier {
  List<Product> _products = [];

  List<Product> get products => _products;

  addCart(Product product) {
    _products.add(product);
    notifyListeners();
  }
}
